﻿using System;
using System.Runtime.CompilerServices;

namespace ConsoleApp4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("- In this application you can either draw a triangle or " +
                "you can search through the yellow pages");
            string choice = "";
            bool runProgram = true;

            //Loops the program, by typing e it breaks the loop and exits the program
            while (runProgram)
            {
                Console.WriteLine("- To search through the yellow pages type \"yellowpages\"");
                Console.WriteLine("- To draw a triangle in the console type \"rectangle\"");
                Console.WriteLine("- To exit the program type e");
                choice = Console.ReadLine();
                
                switch (choice)
                {
                    case "yellowpages":
                        YellowPages.consoleYellowPages();
                        break;
                    case "rectangle":
                        Rectangle.ConsoleRectangle();
                        break;
                    case "e":
                        runProgram = false;
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
