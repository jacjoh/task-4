﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApp4
{
    class Rectangle
    {
        public static void ConsoleRectangle()
        {
            Console.WriteLine("- To draw a rectangle enter the height and length with a space between");
            Console.WriteLine("- To exit the program type \"e\"");

            //Loops the program so that one can draw more rectangles
            while (true)
            {
                string size = Console.ReadLine();

                //exits this function if one types e in the console
                if(size == "e")
                { 
                    break;
                }

                string[] numbers = size.Split(" ");

                if(numbers.Length<2)
                {
                    Console.WriteLine("- You must input two numbers with a space between them");
                }
                //Try to convert the height and length to int
                else
                {
                    bool isParsableHeight = Int32.TryParse(numbers[0], out int heightNumber);
                    bool isParsableLength = Int32.TryParse(numbers[1], out int lengthNumber);
                    if (!isParsableHeight || !isParsableLength)
                    {
                        Console.WriteLine("- You must input two numbers with a space between them");
                    }
                    else
                    {
                        PrintRectangle(lengthNumber, heightNumber);
                        Console.WriteLine("- Write a new size to draw a new rectangle");
                        Console.WriteLine("- write e to exit");
                    }
                }

            }
        }

        private static void PrintRectangle(int length, int height)
        {
            string lineToPrint = "";
            //iterates through the square
            for (int i = 1; i <= height; i++)
            {
                lineToPrint = "";
                for (int j = 1; j <= length; j++)
                {
                    //print a character when it is the first row or column
                    if( i == 1 || i == height || j == 1 || j == length)
                    {
                        lineToPrint += "O";
                    }
                    //the center of the rectangle is empty
                    else
                    {
                        lineToPrint += " ";
                    }
                }
                Console.WriteLine(lineToPrint);
            }
        }
    }
}
