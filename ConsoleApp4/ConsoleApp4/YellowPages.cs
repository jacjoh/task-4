﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace ConsoleApp4
{
    class YellowPages
    {
        public static void consoleYellowPages()
        {
            List<string> contacts = new List<string>() {
                "Jon Andreasen",
                "Arne Arnesen",
                "Lise Lotte",
                "Thea Wolf",
                "Gerald Clark"
            };

            Console.WriteLine("- This is a console based yellow pages app");
            Console.WriteLine("- To search through the contacts type \"s\" and then the contact to search for");
            Console.WriteLine("- To add a new contact type \"a\" and then the full name of the contact");
            Console.WriteLine("- To remove a contact \"r\" and then the full name of the contact ");
            Console.WriteLine("- To exit the program type e");

            string choice = "";

            while (choice != "e")
            {
                string userResponse = Console.ReadLine();

                int indexOfName = userResponse.IndexOf(" ") + 1;
                //the first part of the string is the command, and the rest is the name 
                choice = userResponse.Split(" ")[0];
                string name = userResponse.Substring(indexOfName);

                switch (choice)
                {
                    case "s":
                        SearchContacts(contacts, name);
                        break;
                    case "a":
                        //indexOfName will be 0 when there is no spaces, and then send a as the name to add
                        //So need to check the size of the index before adding
                        if (indexOfName > 0)
                        {
                            AddContact(contacts, name);
                        }
                        else
                        {
                            Console.WriteLine("- You need to write a name to add");
                        }
                        break;
                    case "r":
                        RemoveContact(contacts, name);
                        break;
                    case "e":
                        break;
                    default:
                        Console.WriteLine("- To search through the contacts type \"s\" and then the contact to search for");
                        Console.WriteLine("- To add a new contact type \"a\" and then the full name of the contact");
                        Console.WriteLine("- To remove a contact \"r\" and then the full name of the contact ");
                        Console.WriteLine("- To exit the program type e");
                        break;
                }
            }
        }

        private static void RemoveContact(List<string> contacts, string name)
        {
            //Remove name from contact, and check if it worked
            if (!contacts.Remove(name))
            {
                Console.WriteLine("- There is no such name in your contacts");
                return;
            }
            Console.WriteLine($"- {name} was successfully removed from your contacts");
        }

        private static void AddContact(List<string> contacts, string name)
        {
            //should not add a element if there is no name to add
            if (name.Length == 0)
            {
                Console.WriteLine("- You need to write a name to add");
                return;
            }

            contacts.Add(name);
            Console.WriteLine($"- {name} was successfully added to your contacts");
        }

        private static void SearchContacts(List<string> contacts, string searchString)
        {
            //Collect contacts from the list where the contact name contains the search string
            var Matches = contacts.Where(contact => contact.ToLower().Contains(searchString.ToLower()));

            if (!Matches.Any())
            {
                Console.WriteLine("- There were no results for your search.");
                return;
            }
            Console.WriteLine("- Matches: ");
            foreach (string contact in Matches)
            {
                Console.WriteLine("- " + contact);
            }
        }
    }
}
